<!-- skynet-cli-bash cuz skynet cli is always broken -->
Hello, I made a little Bash script that you can use to upload files to skynet from the terminal. This is very useful for showing log files to developers or making quick backups of remote files through SSH.

To install it you can execute this command:

`sudo curl https://gitlab.com/lukehmcc/skynet-cli-bash/-/raw/master/skynet -o /usr/local/bin/skynet; sudo chmod +x "/usr/local/bin/skynet"`

Explanation: The wget command downloads the script from github and saves it to `/usr/local/bin/skynet` so you can run it from the terminal. Then chmod makes it executable

Then you can upload files from anywhere in the system using

`skynet file.txt`
If you want to uninstall skynet-cli you can run

`sudo rm "/usr/local/bin/skynet"`
Be careful for typos! You don't want to accidentally remove your /usr ;)



